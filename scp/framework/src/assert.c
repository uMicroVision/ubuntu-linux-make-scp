/*
 * Arm SCP/MCP Software
 * Copyright (c) 2020-2021, Arm Limited and Contributors. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <fwk_io.h>

#include <assert.h>
#include <stdint.h>

#if defined(__ARMCC_VERSION)
void __aeabi_assert(const char *expr, const char *file, int line)
{
#ifdef BUILD_MODE_DEBUG
    if (fwk_io_stdout != NULL)
        fwk_io_printf(fwk_io_stdout, "Assert at %s:%d\n", file, line);
#endif

    while (1) {
        continue;
    }
}
#elif defined(__NEWLIB__)
void __assert_func(
    const char *file,
    int line,
    const char *function,
    const char *assertion)
{
#ifdef BUILD_MODE_DEBUG
    if (fwk_io_stdout != NULL)
        fwk_io_printf(fwk_io_stdout, "Assert at %s:%d\n", file, line);
#endif

    while (1) {
        continue;
    }
}
#endif
