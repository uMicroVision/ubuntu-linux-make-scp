# Copyright (c) 2019-2021, ARM Limited and Contributors. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# Neither the name of ARM nor the names of its contributors may be used
# to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

source $DIR/configs/common/common.base

export CROSS_COMPILE_64=${TOP_DIR}/tools/gcc/gcc-arm-10.2-2020.11-x86_64-aarch64-none-linux-gnu/bin/aarch64-none-linux-gnu-
export CROSS_COMPILE=$CROSS_COMPILE_64
export LINUX_COMPILER=$CROSS_COMPILE_64

#Default linux configs per OS
LINUX_CONFIG_LIST=""
BUSYBOX_LINUX_CONFIG_LIST="defconfig"
BUILDROOT_LINUX_CONFIG_LIST="defconfig"

LINUX_TOOLS_IOMMU_BUILD=1

#Grub Build options
GRUB_BUILD_ENABLED=1
GRUB_PATH=grub
GRUB_BUILD_SCRIPT="build-grub.sh "
GRUB_PLAT_CONFIG_FILE=${TOP_DIR}/build-scripts/configs/rdv1mc/grub_config/rdv1mc.cfg

# ARM_TF Flags
ARM_TF_PLATS="rdv1mc"
ARM_TF_DEBUG_ENABLED=1
ARM_TF_ENABLE_SPM=1
ARM_TF_RAS_FW_FIRST=0
ARM_TF_BUILD_FLAGS=""

# Enable support for secure partition, partition manager and RAS.
if [ "$ARM_TF_ENABLE_SPM" == "1" ]; then
	ARM_TF_BUILD_FLAGS="$ARM_TF_BUILD_FLAGS SPM_MM=$ARM_TF_ENABLE_SPM"
	ARM_TF_BUILD_FLAGS="$ARM_TF_BUILD_FLAGS EL3_EXCEPTION_HANDLING=$ARM_TF_ENABLE_SPM"
	if [ "$ARM_TF_RAS_FW_FIRST" == "1" ]; then
		ARM_TF_BUILD_FLAGS="$ARM_TF_BUILD_FLAGS RAS_EXTENSION=1 SDEI_SUPPORT=1 HANDLE_EA_EL3_FIRST=1"
	fi
fi

# Enable multi-chip support by setting the chip count to 4
ARM_TF_BUILD_FLAGS="$ARM_TF_BUILD_FLAGS CSS_SGI_CHIP_COUNT=4"

# Filesystem options
VALID_FILESYSTEMS="busybox buildroot"

#UEFI Options.
UEFI_BUILD_ENABLED=1
if [ "$ARM_TF_ENABLE_SPM" == "1" ]; then
	UEFI_PLATFORMS="rdv1mc rdv1mc_mm_standalone"
else
	UEFI_PLATFORMS="rdv1mc"
fi
declare -A UEFI_PLAT_rdv1mc
UEFI_PLAT_rdv1mc[platname]="RdV1Mc"
UEFI_PLAT_rdv1mc[dsc]="Platform/ARM/SgiPkg/RdV1Mc/RdV1Mc.dsc"
UEFI_PLAT_rdv1mc[output]=css-common
UEFI_PLAT_rdv1mc[defines]="-D EDK2_PLAT=rdv1mc -D VALIDATION_LVL=$VALIDATION_LVL"
if [ "$ARM_TF_RAS_FW_FIRST" == "1" ]; then
	UEFI_PLAT_rdv1mc[defines]="${UEFI_PLAT_rdv1mc[defines]} -D EDK2_ENABLE_HEST -D EDK2_ENABLE_GHES_MM -D EDK2_ENABLE_SDEI"
fi
UEFI_PLAT_rdv1mc[binary]="BL33_AP_UEFI.fd"
UEFI_PLAT_rdv1mc[outbin]=uefi.bin

#Standalone MM Build Options
UEFI_MM_BUILD_ENABLED=$ARM_TF_ENABLE_SPM
declare -A UEFI_PLAT_rdv1mc_mm_standalone
UEFI_PLAT_rdv1mc_mm_standalone[platname]="SgiMmStandalone"
UEFI_PLAT_rdv1mc_mm_standalone[dsc]="Platform/ARM/SgiPkg/PlatformStandaloneMm.dsc"
UEFI_PLAT_rdv1mc_mm_standalone[output]=css-common
UEFI_PLAT_rdv1mc_mm_standalone[binary]="BL32_AP_MM.fd"
UEFI_PLAT_rdv1mc_mm_standalone[outbin]=mm_standalone.bin
if [ "$ARM_TF_RAS_FW_FIRST" == "1" ]; then
       UEFI_PLAT_rdv1mc_mm_standalone[defines]="-D EDK2_ENABLE_GHES_MM"
fi
UEFI_MM_PAYLOAD_BIN="mm_standalone.bin"

#SCP options
SCP_BUILD_ENABLED=1
SCP_PLATFORMS="rdv1mc"
SCP_BUILD_MODE=Debug
SCP_COMPILER_PATH=$TOP_DIR/tools/gcc/gcc-arm-none-eabi-10-2020-q4-major/bin

# Buildroot options
BUILDROOT_DEFCONFIG=aarch64_rdinfra_defconfig

# Misc options
COMPONENT_FLAVOUR=rdv1mc

TARGET_BINS_PLATS="rdv1mc"
declare -A TARGET_rdv1mc
TARGET_rdv1mc[arm-tf]="rdv1mc"
TARGET_rdv1mc[output]="rdv1mc"
TARGET_rdv1mc[uefi]="css-common"
TARGET_rdv1mc[linux]="Image"
TARGET_rdv1mc[ramdisk]=0x88000000
TARGET_rdv1mc[tbbr]=1
TARGET_rdv1mc[scp]="rdv1mc"
SCP_BYPASS_ROM_SUPPORT[rdv1mc]=false
BUILD_SCRIPTS="build-scp.sh build-arm-tf.sh build-uefi.sh build-linux.sh build-busybox.sh build-buildroot.sh build-grub.sh build-target-bins.sh "
