# Copyright (c) 2019-2021, ARM Limited and Contributors. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# Neither the name of ARM nor the names of its contributors may be used
# to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

source $DIR/configs/common/common.base

export CROSS_COMPILE_64=${TOP_DIR}/tools/gcc/gcc-arm-10.2-2020.11-x86_64-aarch64-none-linux-gnu/bin/aarch64-none-linux-gnu-
export CROSS_COMPILE=$CROSS_COMPILE_64
export LINUX_COMPILER=$CROSS_COMPILE_64

#Default linux configs per OS
LINUX_CONFIG_LIST=""
BUSYBOX_LINUX_CONFIG_LIST="defconfig"
BUILDROOT_LINUX_CONFIG_LIST="defconfig"

#Grub Build options
GRUB_BUILD_ENABLED=1
GRUB_PATH=grub
GRUB_BUILD_SCRIPT="build-grub.sh "
GRUB_PLAT_CONFIG_FILE=${TOP_DIR}/build-scripts/configs/rde1edge/grub_config/rde1edge.cfg

# ARM_TF Flags
ARM_TF_PLATS="rde1edge"
ARM_TF_DEBUG_ENABLED=1
ARM_TF_ENABLE_SPM=1
ARM_TF_RAS_FW_FIRST=0
ARM_TF_BUILD_FLAGS=""

# Enable support for secure partition, partition manager and RAS.
if [ "$ARM_TF_ENABLE_SPM" == "1" ]; then
	ARM_TF_BUILD_FLAGS="$ARM_TF_BUILD_FLAGS SPM_MM=$ARM_TF_ENABLE_SPM"
	ARM_TF_BUILD_FLAGS="$ARM_TF_BUILD_FLAGS EL3_EXCEPTION_HANDLING=$ARM_TF_ENABLE_SPM"
	if [ "$ARM_TF_RAS_FW_FIRST" == "1" ]; then
		ARM_TF_BUILD_FLAGS="$ARM_TF_BUILD_FLAGS RAS_EXTENSION=1 SDEI_SUPPORT=1 HANDLE_EA_EL3_FIRST=1"
	fi
fi

# Filesystem options
VALID_FILESYSTEMS="busybox buildroot"

#UEFI Options.
UEFI_BUILD_ENABLED=1
if [ "$ARM_TF_ENABLE_SPM" == "1" ]; then
	UEFI_PLATFORMS="rde1edge rde1edge_mm_standalone"
else
	UEFI_PLATFORMS="rde1edge"
fi
declare -A UEFI_PLAT_rde1edge
UEFI_PLAT_rde1edge[platname]="RdE1Edge"
UEFI_PLAT_rde1edge[dsc]="Platform/ARM/SgiPkg/RdE1Edge/RdE1Edge.dsc"
UEFI_PLAT_rde1edge[output]=css-common
UEFI_PLAT_rde1edge[defines]="-D EDK2_PLAT=rde1edge -D VALIDATION_LVL=$VALIDATION_LVL"
if [ "$ARM_TF_RAS_FW_FIRST" == "1" ]; then
	UEFI_PLAT_rdn1edge[defines]="${UEFI_PLAT_rdn1edge[defines]} -D EDK2_ENABLE_HEST -D EDK2_ENABLE_GHES_MM -D EDK2_ENABLE_SDEI"
fi
UEFI_PLAT_rde1edge[binary]="BL33_AP_UEFI.fd"
UEFI_PLAT_rde1edge[outbin]=uefi.bin

#Standalone MM Build Options
UEFI_MM_BUILD_ENABLED=$ARM_TF_ENABLE_SPM
declare -A UEFI_PLAT_rde1edge_mm_standalone
UEFI_PLAT_rde1edge_mm_standalone[platname]="SgiMmStandalone"
UEFI_PLAT_rde1edge_mm_standalone[dsc]="Platform/ARM/SgiPkg/PlatformStandaloneMm.dsc"
UEFI_PLAT_rde1edge_mm_standalone[output]=css-common
if [ "$ARM_TF_RAS_FW_FIRST" == "1" ]; then
       UEFI_PLAT_rdn1edge_mm_standalone[defines]="-D EDK2_ENABLE_GHES_MM -D EDK2_ENABLE_DMC620_MM"
fi
UEFI_PLAT_rde1edge_mm_standalone[binary]="BL32_AP_MM.fd"
UEFI_PLAT_rde1edge_mm_standalone[outbin]=mm_standalone.bin
UEFI_MM_PAYLOAD_BIN="mm_standalone.bin"

#SCP options
SCP_BUILD_ENABLED=1
SCP_PLATFORMS="rdn1e1"
SCP_BUILD_MODE=Debug
SCP_COMPILER_PATH=$TOP_DIR/tools/gcc/gcc-arm-none-eabi-10-2020-q4-major/bin

# Buildroot options
BUILDROOT_DEFCONFIG=aarch64_rdinfra_defconfig

# Misc options
COMPONENT_FLAVOUR=rde1edge

TARGET_BINS_PLATS="rde1edge"
declare -A TARGET_rde1edge
TARGET_rde1edge[arm-tf]="rde1edge"
TARGET_rde1edge[output]="rde1edge"
TARGET_rde1edge[uefi]="css-common"
TARGET_rde1edge[linux]="Image"
TARGET_rde1edge[ramdisk]=0x88000000
TARGET_rde1edge[tbbr]=1
TARGET_rde1edge[scp]="rdn1e1"
SCP_BYPASS_ROM_SUPPORT[rde1edge]=false
BUILD_SCRIPTS="build-arm-tf.sh build-uefi.sh build-linux.sh build-busybox.sh build-buildroot.sh build-grub.sh build-scp.sh build-target-bins.sh "

# UEFI SCT Options
SCT_OUT_DIR=$TOP_DIR/output/rde1edge/uefisct
SCT_BUILD_MODE=DEBUG
